#!/usr/bin/env python3

import json
import urllib.parse
import urllib.request
import sys
import os.path

API="https://gitlab.com/api/v4%s"

def request(cmd):
    request = urllib.request.Request(API % cmd, headers={"PRIVATE-TOKEN" : token})
    request.get_method = lambda: 'GET'
    return json.load(urllib.request.urlopen(request))

def delete(cmd):
    request = urllib.request.Request(API % cmd, headers={"PRIVATE-TOKEN" : token})
    request.get_method = lambda: 'DELETE'
    urllib.request.urlopen(request)

def post(cmd, data):
    request = urllib.request.Request(API % cmd, headers={"PRIVATE-TOKEN" : token}, data=urllib.parse.urlencode(data).encode("utf-8"))
    request.get_method = lambda: 'POST'
    urllib.request.urlopen(request)

def add_badge(id, link, image):
    icon = {
         'link_url': link,
         'image_url': image
    }
    post("/projects/%d/badges" % id, icon)

token = ""

if len(sys.argv) > 1:
    token = sys.argv[1]
else:
    try:
        with open(os.path.expandvars('$HOME/.config/gitlab'), 'r') as f:
            token = f.read().strip()
    except IOError:
        pass

if token == "":
    print("error: set token")
    exit(1)

payments = ["paypal.com", "paypalobjects.com", "liberapay.com", "yandex.ru", "paypal.me", "patreon.com", "axet.gitlab.io/donate", "yoomoney.ru"]
full = ["http://axet.gitlab.io/"]

page = 1
while True:
    pp = request("/projects?page=%d&owned=true&simple=true&visibility=public" % page)
    if len(pp) == 0:
        break
    for p in pp:
        print(p['path'])
        bb = request("/projects/%d/badges" % p['id'])
        dd = set()
        for b in bb:
            for s in payments:
                if s in b['link_url'] or s in b['image_url']:
                    dd.add(b['id'])
            for s in full:
                if s == b['link_url'] or s == b['image_url']:
                    dd.add(b['id'])
        for d in dd:
            delete("/projects/%d/badges/%d" % (p['id'], d))
        if "android-" in p['path']:
            add_badge(p['id'], 'http://axet.gitlab.io/', 'http://axet.gitlab.io/badges/gitlab-logo.svg')
        #add_badge(p['id'], 'http://axet.gitlab.io/donate.html', 'http://axet.gitlab.io/badges/donate.svg')
        #add_badge(p['id'], 'https://liberapay.com/axet/', 'https://img.shields.io/liberapay/patrons/axet.svg?logo=liberapay&label=liberapay')
        #add_badge(p['id'], 'https://www.paypal.me/axru/usd', 'http://axet.gitlab.io/badges/paypal-usd.svg')
        #add_badge(p['id'], 'https://www.paypal.me/axru/eur', 'http://axet.gitlab.io/badges/paypal-eur.svg')
        #add_badge(p['id'], 'https://www.paypal.me/axru/rub', 'http://axet.gitlab.io/badges/paypal-rub.svg')
        add_badge(p['id'], 'http://axet.gitlab.io/donate.html', 'http://axet.gitlab.io/badges/donate.svg')
    page = page + 1
